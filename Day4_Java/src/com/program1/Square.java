package com.program1;

public class Square extends Shape{

	private Integer side;
	public Square(String name) {
		super(name);
	}

	public Square(String name, Integer side) {
		super(name);
		this.side = side;
	}

	public Integer getSide() {
		return side;
	}

	public void setSide(Integer side) {
		this.side = side;
	}

	@Override
	public float calculateArea() {
		float area=side*side;
		return area;
	}

}
