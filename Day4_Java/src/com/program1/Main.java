package com.program1;

public class Main {
public static void main(String[] args) {
	Circle circle=new Circle("circle",12);
	Square square=new Square("square",33);
	Rectangle rectangle=new Rectangle("rectangle",12,32);
	
	float ac=circle.calculateArea();
	float as=square.calculateArea();
	float ar=rectangle.calculateArea();
	System.out.println("area of circle= "+ac);
	System.out.println("area of square= "+as);
	System.out.println("area of rectangle= "+ar);
}
}
