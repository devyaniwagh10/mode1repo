package com.program4;

public class Swap {
	int a = 1;
	int b = 3;
	int c;

	public void swapMethod() {
		System.out.println("before swapping " +a+" "+b);
		c = a;
		a = b;
		b = c;
		System.out.println("after swapping "+a+" "+b);
		
	}
}
