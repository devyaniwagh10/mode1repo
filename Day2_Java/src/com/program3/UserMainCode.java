package com.program3;

public class UserMainCode {
	public static int m1(String sentence) {
		int count = 0;

		for (int i = 0; i < sentence.length(); i++) {
			char ch = sentence.charAt(i);
			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
				count++;
			}
			
		}
		
		return count;
	}
}
