package com.program3;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Enter a sentence :");
		Scanner sc = new Scanner(System.in);
		String sentence = sc.nextLine();
		int count = UserMainCode.m1(sentence);
		System.out.println("Number of vowels in the given sentence is "+count);
	}

}
