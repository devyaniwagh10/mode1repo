package com.program4;

public class Room {
	private String roomNo;
	private String roomType;
	private String roomArea;
	private String aMachine;

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public void setRoomArea(String roomArea) {
		this.roomArea = roomArea;
	}

	public void setaMachine(String aMachine) {
		this.aMachine = aMachine;
	}

	public String display()
	{
		return roomNo+" "+roomArea+" "+roomType+" "+aMachine;
	}
}
