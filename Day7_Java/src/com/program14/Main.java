package com.program14;

import java.text.ParseException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the date (dd-MM-yyyy)");
		String date = sc.next();
		String newOut;
		try {
			newOut = UserMainCode.convertDateFormat(date);
			System.out.println(newOut);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

}
