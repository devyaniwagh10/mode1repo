package com.program14;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserMainCode {

	public static String convertDateFormat(String s) throws ParseException
	{
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		 Date date = formatter.parse(s);
		 String  newDate = formatter.format(date);
		return newDate;
		
		
	}
}
