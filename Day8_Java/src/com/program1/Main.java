package com.program1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** BufferedReader class  to prompt a user to input his/her name and then the output will be shown**/
public class Main {
public static void main(String[] args) {
	
	BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("enter the input");
	try {
		String s=in.readLine();
		System.out.println(s);
	} catch (IOException e) {
		e.printStackTrace();
	}
	
}
}
