package com.program5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
/** to read longest word**/
public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		new Main().findLongestWords();
	}
		public String findLongestWords() throws FileNotFoundException {

		       String longest_word = "";
		       String current;
		       Scanner sc = new Scanner(new File("program5.txt"));


		       while (sc.hasNext()) {
		          current = sc.next();
		           if (current.length() > longest_word.length()) {
		             longest_word = current;
		           }

		       }
		         System.out.println("\n"+longest_word+"\n");
		            return longest_word;
		            }
}
