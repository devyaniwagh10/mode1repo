package com.program2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/** to read and write file**/
public class Main {
	public static void main(String[] args) {
	try {
		FileWriter fileWriter = new FileWriter("program2.txt");
		BufferedWriter bufferedWriter=new BufferedWriter(fileWriter);
		bufferedWriter.write("hello Hcl people");
		bufferedWriter.flush();
		bufferedWriter.close();
	} catch (IOException e) {
		e.printStackTrace();
	}
	FileReader fileWriter;
	try {
		fileWriter = new FileReader("program2.txt");
		BufferedReader bufferedReader=new BufferedReader(fileWriter);
		try {
			String line=bufferedReader.readLine();
			while(line !=null)
			{
				System.out.println(line);
				line=bufferedReader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}

	}
}
