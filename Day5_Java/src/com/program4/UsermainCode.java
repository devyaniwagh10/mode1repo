package com.program4;

public class UsermainCode {
	
	public static int age(int a) throws AgeInvalidException
	{
		if(a<19)
	    throw new AgeInvalidException("age shouldbe greater than 19");
		return a;
	}

}
