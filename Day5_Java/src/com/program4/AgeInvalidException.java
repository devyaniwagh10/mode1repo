package com.program4;

public class AgeInvalidException extends Exception {
	
	
	public AgeInvalidException(String msg)
	{
		super(msg);
	}

}
