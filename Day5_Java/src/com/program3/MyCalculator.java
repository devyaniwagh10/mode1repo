package com.program3;

public class MyCalculator {
	public  long power(int a, int b) throws Exception
	{
        if(a==0 && b==0)
            throw new Exception("a and b should not be zero");
        
        else if(a<0 && a<0)
        {
        	throw new Exception("a and b should not be 0");
        }
        else if(a<0 || b<0)
        {
        	throw new Exception("a or b should not be zero");
        }
		return (long) Math.pow((double)a,(double)b);
		
	}

}
